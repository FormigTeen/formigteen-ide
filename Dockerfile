#
#--------------------------------------------------------------------------
# Image Setup
#--------------------------------------------------------------------------
#
# To edit the 'workspace' base Image, visit its repository on Github
#    https://github.com/Laradock/workspace
#
# To change its version, see the available Tags on the Docker Hub:
#    https://hub.docker.com/r/laradock/workspace/tags/
#

FROM fedora:latest
LABEL maintainer="Matheus Silva Freitas <msformigteen@live.com>"

###########################################################################
# Register non-root user and Update System
###########################################################################

# Add a non-root user to prevent files being created with root permissions on host machine.
ARG PUID=1000
ENV PUID ${PUID}
ARG PGID=1000
ENV PGID ${PGID}

ARG USER_NONROOT=formigteen

RUN dnf update -y && \
        dnf upgrade -y && \
        groupadd -g ${PGID} ${USER_NONROOT} && \
        useradd -u ${PUID} -g ${USER_NONROOT} -m ${USER_NONROOT} -G root && \
        usermod -p ${USER_NONROOT} ${USER_NONROOT} && \
        dnf install -y wget unzip

###########################################################################
# Install ZSH, Vim, Tmux, Tmuxinator, Git
###########################################################################

RUN curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/install.sh | sh

###########################################################################
# Configure root User
###########################################################################

USER root

# Configure ZSH and Set Default Shell
RUN curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/zsh.sh | sh && \
        usermod -s /bin/zsh ${USER_NONROOT} && \
        usermod -s /bin/zsh root && \
    # Configure TMux 
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/tmux.sh | sh && \
    # Configure Vim
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/vim.sh | sh

###########################################################################
# Configure noroot User
###########################################################################

USER ${USER_NONROOT}

# Configure ZSH
RUN curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/zsh.sh | sh && \
    # Configure TMux 
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/tmux.sh | sh && \
    # Configure Vim
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/vim.sh | sh

WORKDIR ~

ENTRYPOINT ["/bin/zsh"]

