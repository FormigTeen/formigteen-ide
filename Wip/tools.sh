#Install Lpass

EMAIL_LPASS="msformigteen@live.com"

sudo dnf install -y lastpass-cli
lpass login $EMAIL_LPASS

#Install Snap

sudo dnf -y install snapd
sudo ln -s /var/lib/snapd/snap /snap

# Install PHPStorm and Webstorm

sudo snap install phpstorm --classic
sudo snap install webstorm --classic

# Install Insomnia

sudo snap install insomnia

#Install Gnome Tweaks

BACKUP_DIRECTORY="~/.formigteen/backup"

sudo dnf install -y gnome-tweaks

#Backup GNOME Extensions
cp "$HOME/.local/share/gnome-shell/extensions" "$BACKUP_DIRECTORY/extensions"
