#
#--------------------------------------------------------------------------
# Image Setup
#--------------------------------------------------------------------------
#
# To edit the 'workspace' base Image, visit its repository on Github
#    https://github.com/Laradock/workspace
#
# To change its version, see the available Tags on the Docker Hub:
#    https://hub.docker.com/r/laradock/workspace/tags/
#

FROM fedora:latest
LABEL maintainer="Matheus Silva Freitas <msformigteen@live.com>"

###########################################################################
# Register non-root user and Update System
###########################################################################

RUN dnf update -y && \
        dnf upgrade -y && \
        dnf install -y wget unzip

###########################################################################
# Install ZSH, Vim, Tmux, Tmuxinator, Git
###########################################################################

RUN curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/install.sh | sh

###########################################################################
# Configure root User
###########################################################################

USER root

# Configure ZSH and Set Default Shell
RUN curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/zsh.sh | sh && \
        usermod -s /bin/zsh root && \
    # Configure TMux 
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/tmux.sh | sh && \
    # Configure Vim
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/vim.sh | sh

WORKDIR ~

ENTRYPOINT ["/bin/zsh"]

