#!/bin/bash

# Update Package
dnf -y update && \
	dnf -y upgrade && \
	dnf -y install dnf-plugins-core -y && \
	dnf -y config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo && \
	dnf -y install docker-ce docker-ce-cli containerd.io grubby docker-compose && \
	usermod -a -G docker $USER && \
	grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0" && \
	reboot
