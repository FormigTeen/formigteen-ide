#!/bin/sh

function net_host()
{
    ip -4 addr | grep -oP '(?<=inet\s)\d+(\.\d+){3}'
}

function net_local()
{
    for ip in $1.{1..$2}
    do
        ping -c 1 -W 1 $ip | grep -q "64 bytes" && echo $ip
    done
}

