echo "All Functions Installed!"

function formigteen_get()
{
    source <(curl -fsSL "ide.formigteen.dev/$1")
}

function formigteen_hi()
{
    for i in {1..1000}
    do
        echo "FormigTeen $i passando para dar um Oi :)\n"
    done
}

function formigteen_install()
{
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/install.sh | sh
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/zsh.sh | sh
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/tmux.sh | sh
    curl -fsSL https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/scripts/vim.sh | sh
}

