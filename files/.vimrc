set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'valloric/youcompleteme'
Plugin 'yggdroot/indentline'
Plugin 'tomasr/molokai'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'inside/vim-search-pulse'
Plugin 'itchyny/vim-cursorword'
Plugin 'CursorLineCurrentWindow'
Plugin 'andrewradev/splitjoin.vim'
Plugin 'matze/vim-move'
Plugin 'myusuf3/numbers.vim'
Plugin 'sheerun/vim-polyglot'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'shougo/vimproc.vim'
Plugin 'shougo/unite.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'xuyuanp/nerdtree-git-plugin'
Plugin 'thaerkh/vim-workspace'
Plugin 'yuttie/comfortable-motion.vim'
Plugin 'mbbill/undotree'
Plugin 't9md/vim-choosewin'
Plugin 'machakann/vim-highlightedyank'
Plugin 'reedes/vim-wheel'
Plugin 'posva/vim-vue'
Plugin 'turbio/bracey.vim'
Plugin 'easymotion/vim-easymotion'
Plugin 'mattn/webapi-vim'
Plugin 'leafOfTree/vim-vue-plugin'
Plugin 'L9'
Plugin 'matchit.zip'
Plugin 'FuzzyFinder'
Plugin 'Kshenoy/vim-signature'
Plugin 'airblade/vim-gitgutter'


" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'

" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" End Vundle

" Configure Vim-Javascrip Plugin
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END

" Configure Numbers Plugin
nnoremap <F3> :NumbersToggle<CR>
nnoremap <F4> :NumbersOnOff<CR>

" Configure Undotree
nnoremap <F5> :UndotreeToggle<cr>

" Configure EasyMotion PLugin
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

" N search Caracters
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

" These `n` & `N` mappings are options. You do not have to map `n` & `N` to EasyMotion.
" Without these mappings, `n` & `N` works fine. (These mappings just provide
" different highlight method and have some other features )
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

" Arrow Motions
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)

let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

" Configure EasyMotion to SmartCase
let g:EasyMotion_smartcase = 1

" Require tpope/vim-repeat to enable dot repeat support
" Jump to anywhere with only `s{char}{target}`
" `s<CR>` repeat last find motion.
nmap s <Plug>(easymotion-s)
" Bidirectional & within line 't' motion
omap t <Plug>(easymotion-bd-tl)
" Use uppercase target labels and type as a lower case
let g:EasyMotion_use_upper = 1
" type `l` and match `l`&`L`
let g:EasyMotion_smartcase = 1
" Smartsign (type `3` and match `3`&`#`)
let g:EasyMotion_use_smartsign_us = 1

" Configure NERDTree Plugin
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && v:this_session == "" | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Configure ChooseWin Plugin
nmap  -  <Plug>(choosewin)
let g:choosewin_overlay_enable = 1

" Configure IndentLine Plugin
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
" Configure Unimpaired.vim Plugin

" Configure Bracey Plugin
let g:bracey_eval_on_save = 1
" Configure Bracey Plugin

" Configure Emmet Plugin
let g:user_emmet_settings = webapi#json#decode(join(readfile(expand('~/.vim_emmet_custom.json')), "\n"))
" Configure Emmet Plugin

" Configure Vim Signature Plugin
let g:SignatureMarkTextHLDynamic = 1
" Configure Vim Signature Plugin

" Configure CtrlP Plugin
let g:ctrlp_show_hidden = 1
let g:ctrlp_max_files = 0
let g:ctrlp_regexp = 1
" Configure CtrlP Plugin

set cursorline
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>

" Configure ColorScheme 
silent! colorscheme molokai

set tabstop=4
set shiftwidth=4
set expandtab
set number
set wildmode=longest,list 
set history=10000
set encoding=UTF-8
set autoindent

" Consult Tipo #64 to Review this Option
set pastetoggle=<f6>

" Configure Expand %%
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h'). '/' : '%%'

"Configure Alt Key to Terminal OS ( This's is Fix a NeoVim )
let c='a'
while c <= 'z'
    exec "set <A-".c.">=\e".c
    exec "imap \e".c." <A-".c.">"
    let c = nr2char(1+char2nr(c))
endw

set timeout ttimeoutlen=50

:command Sudow write !sudo tee % > /dev/null
