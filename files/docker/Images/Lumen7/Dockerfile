FROM trafex/alpine-nginx-php7

###########################################################################
# Install Dependecies
###########################################################################

USER root

RUN apk --no-cache add php7-tokenizer php7-xmlwriter php7-pdo_pgsql php7-pgsql gettext

ARG PORT=8080
ENV PORT=${PORT}

RUN mkdir -p /.config/psysh && chown -R nobody:nobody /.config && chmod 777 -R /.config /.dockerenv

RUN sed -i 's|root /var/www/html;|root /var/www/html/public;|g; \
        s|8080|${PORT}|g; \
        /[::]${PORT}/d' /etc/nginx/nginx.conf

RUN sed 's|root /var/www/html;|root /var/www/html/public;|g; \
        s|8080|$PORT|g' /etc/nginx/nginx.conf > /etc/nginx/nginx.conf.template && \
        sed -i "s|^command=nginx -g 'daemon off;'$|command=/bin/sh -c \"envsubst '\$PORT' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf \&\& nginx -g 'daemon off;'\"|g" /etc/supervisor/conf.d/supervisord.conf && \
        chown -R nobody:nobody /etc/nginx /etc/supervisor/conf.d/supervisord.conf 

ARG APP_NAME="Auth MicroService" 
ENV APP_NAME=${APP_NAME} 

ARG APP_ENV=production 
ENV APP_ENV=${APP_ENV} 

ARG APP_DEBUG=true 
ENV APP_DEBUG=${APP_DEBUG}

ARG APP_URL=auth.test 
ENV APP_URL=${APP_URL}

ARG DATABASE_URL=mysql://root:password@127.0.0.1/forge?charset=UTF-8
ENV DATABASE_URL=${DATABASE_URL} 

ARG QUEUE_CONNECTION=database 
ENV QUEUE_CONNECTION=${QUEUE_CONNECTION} 

ARG CACHE_DRIVER=file
ENV CACHE_DRIVER=${CACHE_DRIVER}

ARG MAIL_DRIVER=smtp
ENV MAIL_DRIVER=${MAIL_DRIVER}

ARG MAIL_HOST=null
ENV MAIL_HOST=${MAIL_HOST}

ARG MAIL_FROM_ADDRESS=auth@auth.com
ENV MAIL_FROM_ADDRESS=${MAIL_FROM_ADDRESS}

ARG MAIL_FROM_NAME=AuthService
ENV MAIL_FROM_NAME=${MAIL_FROM_NAME}

ARG MAIL_ENCRYPTION=tls
ENV MAIL_ENCRYPTION=${MAIL_ENCRYPTION}

ARG MAIL_USERNAME=auth@auth.com
ENV MAIL_USERNAME=${MAIL_USERNAME}

ARG MAIL_PASSWORD=secret
ENV MAIL_PASSWORD=${MAIL_PASSWORD}

ARG PASSPORT_EXPIRES_TOKEN_DAYS=15
ENV PASSPORT_EXPIRES_TOKEN_DAYS=${PASSPORT_EXPIRES_TOKEN_DAYS}

ARG PASSPORT_REFRESH_TOKEN_DAYS=15
ENV PASSPORT_REFRESH_TOKEN_DAYS=${PASSPORT_REFRESH_TOKEN_DAYS}

ARG LUMEN_PASSPORT_REFRESH_TOKEN_YEARS=15
ENV LUMEN_PASSPORT_REFRESH_TOKEN_YEARS=${LUMEN_PASSPORT_REFRESH_TOKEN_YEARS}

USER nobody

COPY ./app /var/www/html

###########################################################################
# Install Composer
###########################################################################

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN cd /var/www/html && \
        composer install --optimize-autoloader --no-interaction --no-progress && \
        cp .env.example .env && \
        php artisan key:generate

EXPOSE 8080 

VOLUME /var/www/html/storage

