#!/bin/bash
###########################################################################
# Variables Script
###########################################################################
FOLDER_BACKUP=~/.formigteen/backup/
TMUX_CONF_FILE=~/.tmux.conf
TMUX_CONF_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.tmux.conf"
TMP_PLUGIN_URL="https://github.com/tmux-plugins/tpm"

###########################################################################
# Configure Tmux
###########################################################################

# Backup Tmux Conf File
if [ -f "$TMUX_CONF_FILE" ]; then
    echo "Backup Tmux Conf File Finished"
    mkdir -p "$FOLDER_BACKUP"
    cp "$TMUX_CONF_FILE" "$FOLDER_BACKUP"
fi

# Add New Tmux Conf File
curl -fsSL  "$TMUX_CONF_URL" --output "$TMUX_CONF_FILE"

# Install Plugin Manager to TMUX
 git clone "$TMP_PLUGIN_URL" ~/.tmux/plugins/tpm

# Install Plugins TPM
~/.tmux/plugins/tpm/bin/install_plugins
