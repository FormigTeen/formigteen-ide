#!/bin/bash
###########################################################################
# Variables Script
###########################################################################
FOLDER_BACKUP=~/.formigteen/backup/
OH_MY_ZSH_URL="https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh"
PRE_OWN_RC_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.preownrc"
POS_OWN_RC_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.posownrc"
ENV_RC_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.envrc"
PRE_OWN_RC_FILE=~/.preownrc
POS_OWN_RC_FILE=~/.posownrc
ENV_RC_FILE=~/.envrc
ZSH_RC_FILE=~/.zshrc
ZSH_SYNTAX_HIGHLIGHTING_PLUGIN_URL=https://github.com/zsh-users/zsh-syntax-highlighting.git
ZSH_AUTOSUGGESTIONS_PLUGIN_URL=https://github.com/zsh-users/zsh-autosuggestions
ZSH_FZF_PLUGIN_URL="https://github.com/junegunn/fzf.git"
ZSH_HACK_FONT_URL="https://github.com/ryanoasis/nerd-fonts/releases/download/v1.2.0/Hack.zip"
ZSH_POWERLEVEL_9K_URL="https://github.com/bhilburn/powerlevel9k.git"

###########################################################################
# Configure ZSH
###########################################################################

# Backup ZSH RC File
if [ -f "$ZSH_RC_FILE" ]; then
    echo "Backup ZSH RC File Finished"
    mkdir -p "$FOLDER_BACKUP"
    cp "$ZSH_RC_FILE" "$FOLDER_BACKUP"
fi

# Execute Script to Configure Oh-My-Zsh
curl -fsSL "$OH_MY_ZSH_URL" | sh

# Add Custom RC File Script 
curl -fsSL  "$PRE_OWN_RC_URL" --output "$PRE_OWN_RC_FILE"
curl -fsSL  "$POS_OWN_RC_URL" --output "$POS_OWN_RC_FILE"
curl -fsSL  "$ENV_RC_URL" --output "$ENV_RC_FILE"
curl -fsSL  "$ENV_RC_URL" --output "$ENV_RC_FILE"

# Add Bootstrap to Pre OwnRC to ZSH RC
sed -ir "\|^source \$ZSH/oh-my-zsh.sh$|i \                        \

    if [ -f $ENV_RC_FILE ]; then        \n\
        . $ENV_RC_FILE                  \n\
    fi                                      \n\

    if [ -f $PRE_OWN_RC_FILE ]; then        \n\
        . $PRE_OWN_RC_FILE                  \n\
        fi                                  \n\
                                            \n\n\
" "$ZSH_RC_FILE"

# Add Bootstrap to Pos OwnRC to ZSH RC
sed -ir "\|^source \$ZSH/oh-my-zsh.sh$|a \                        \
                                            \n\n\
    if [ -f $POS_OWN_RC_FILE ]; then        \n\
        . $POS_OWN_RC_FILE                  \n\
    fi                                      \n\
" "$ZSH_RC_FILE"

# Install Syntax Highlighting Plugin
git clone  $ZSH_SYNTAX_HIGHLIGHTING_PLUGIN_URL ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting 

# Install Syntax AutoSuggestions Plugin
git clone $ZSH_AUTOSUGGESTIONS_PLUGIN_URL ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Instala o FZF Plugin
git clone --depth 1 $ZSH_FZF_PLUGIN_URL ~/.fzf && ~/.fzf/install --all

# Install Hack Font to Terminal
mkdir ~/.fonts
wget -O /tmp/hack-font $ZSH_HACK_FONT_URL
unzip -d ~/.fonts /tmp/hack-font
rm /tmp/hack-font

# Install Theme ZSH Powerline 
git clone $ZSH_POWERLEVEL_9K_URL ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel9k
