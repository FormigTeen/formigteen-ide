#!/bin/bash
###########################################################################
# Variables Script
###########################################################################
FOLDER_BACKUP=~/.formigteen/backup/
VIM_CONF_FILE=~/.vimrc
VIM_CONF_EMMET_FILE=~/.vim_emmet_custom.json
VIM_CONF_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.vimrc"
VIM_CONF_EMMET_URL="https://gitlab.com/FormigTeen/formigteen-ide/-/raw/master/files/.vim_emmet_custom.json"
VUNDLE_PLUGIN_URL="https://github.com/VundleVim/Vundle.vim.git"

###########################################################################
# Configure Vim
###########################################################################

# Backup Tmux Conf File
if [ -f "$VIM_CONF_FILE" ]; then
    echo "Backup Vim Conf File Finished"
    mkdir -p "$FOLDER_BACKUP"
    cp "$VIM_CONF_FILE" "$FOLDER_BACKUP"
fi

# Install Plugin Manager to Vim
git clone "$VUNDLE_PLUGIN_URL" ~/.vim/bundle/Vundle.vim

# Add New Vim Conf File
curl -fsSL  "$VIM_CONF_URL" --output "$VIM_CONF_FILE"

# Add New Vim Emmet Conf File
curl -fsSL  "$VIM_CONF_EMMET_URL" --output "$VIM_CONF_EMMET_FILE" 

# Install Vundle Plugin
vim +PluginInstall +qall

# Configure YouCompleteMe
cd ~/.vim/bundle/youcompleteme
python3 install.py --all
cd -

# Configure Bracey
cd ~/.vim/bundle/bracey.vim
npm install --prefix server
cd -
